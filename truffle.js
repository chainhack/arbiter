require('babel-register')({
    ignore: /node_modules\/(?!openzeppelin-solidity\/test\/helpers)/
});

module.exports = {
    networks: {
        development: {
            host: "127.0.0.1",
            port: 9545,
            network_id: "4447",
            // gas: 4700000
            // gas: 0xffffffffff
        },
        local: {
            host: "127.0.0.1",
            port: 8545,
            network_id: "10"
        },
        // rinkeby: {
        //     host: "",
        //     port: 8545,
        //     network_id: "4",
        //     from: ""
        // },
        ganache: {
            host: "ganache",
            port: 8545,
            network_id: "5777"
        }
    }
};
