pragma solidity ^0.4.24;

import "./Miner.sol";
import "./Coder.sol";


// This is the Arbiter contract for ChainHack BDD enforcing via blockchain.

contract Arbiter is Coder, Miner {
	mapping (uint256 => string) tasks;

	event TaskAdded(address indexed _from, uint256 _id, string _value);

	constructor() public {
	}

	//TODO add monetary reward
	function addTask(uint256 _id, string _task)
	public
	payable {
		require(bytes(tasks[_id]).length == 0);
		tasks[_id] = _task;
		emit TaskAdded(msg.sender, _id, _task);
	}

	function getTask(uint256 _id)
	public
	view
	returns (string task){
		task = tasks[_id];
	}

}
