pragma solidity ^0.4.0;

contract Coder {
    //           coder               taskId     solutionId
    mapping(address => mapping(uint256 => uint256)) solutions;
    //           solutionId solution
    mapping(uint256 => string) solutionById;

    function addSolution(uint256 _taskId, uint256 _id, string _solution)
    public
    payable {
        require(bytes(solutionById[_id]).length == 0);
        require(bytes(_solution).length > 0);

        solutionById[_id] = _solution;
        solutions[msg.sender][_taskId] = _id;
        //TODO add the task-solution pair to mining queue
    }

    function getSolution(uint256 _id)
    public
    view
    returns (string solution){
        solution = solutionById[_id];
    }
}
