pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/access/Roles.sol";

contract AdminRole {
    using Roles for Roles.Role;

    event AdminAdded(address indexed account);

    Roles.Role private admins;

    constructor() internal {
        _addAdmin(msg.sender);
    }

    modifier onlyAdmin() {
        require(isAdmin(msg.sender));
        _;
    }

    function isAdmin(address account) public view returns (bool) {
        return admins.has(account);
    }

    function addAdmin(address account) public onlyAdmin {
        _addAdmin(account);
    }

    function _addAdmin(address account) internal {
        admins.add(account);
        emit AdminAdded(account);
    }
}

