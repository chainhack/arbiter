pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/access/Roles.sol";
import "./AdminRole.sol";

contract MinerRole is AdminRole {
    using Roles for Roles.Role;

    event MinerAdded(address indexed account);
    event MinerRemoved(address indexed account);

    Roles.Role private miners;

    modifier onlyMiner() {
        require(isMiner(msg.sender));
        _;
    }

    function isMiner(address account) public view returns (bool) {
        return miners.has(account);
    }

    function addMiner(address account) public onlyAdmin {
        _addMiner(account);
    }

    function renounceMiner() public {
        _removeMiner(msg.sender);
    }

    function _addMiner(address account) internal {
        miners.add(account);
        emit MinerAdded(account);
    }

    function _removeMiner(address account) internal {
        miners.remove(account);
        emit MinerRemoved(account);
    }
}

