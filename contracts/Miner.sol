pragma solidity ^0.4.0;

import "./roles/AdminRole.sol";
import "./roles/MinerRole.sol";

contract Miner is AdminRole, MinerRole {

    function getTaskSolutionPair()
    public
    onlyMiner
    view
    returns (uint256[] taskIds, uint256[] solutionIds)
    {
        taskIds = new uint256[](1);
        solutionIds = new uint256[](42);
    }
}
