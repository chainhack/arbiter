import 'babel-polyfill';
import {reverting} from 'openzeppelin-solidity/test/helpers/shouldFail';

var Arbiter = artifacts.require("./Arbiter.sol");

var accounts;
var arbiter;

contract('Coder', function(accs) {
    it("coders are able to submit solutions", async function() {
        accounts = accs;
        let miner = accounts[2];
        let coder = accounts[3];

        arbiter = await Arbiter.deployed();

        await arbiter.addTask(1, "my python generators; my python tests",
            {from: miner});

        await arbiter.addSolution(1, 42, "my python solution for task 1")
    });

    // it("tasks can't be redefined", async function() {
    //     let miner = accounts[2];
    //     await reverting(
    //         arbiter.addTask(1, "other generators and tests",
    //             {from: miner})
    //     );
    // });
    });
