import 'babel-polyfill';
import {reverting} from 'openzeppelin-solidity/test/helpers/shouldFail';


var Arbiter = artifacts.require("./Arbiter.sol");

var accounts;
var arbiter;

contract('Arbiter', function(accs) {
  it("miners are able to submit tasks", async function() {
      accounts = accs;
      let miner = accounts[2];
      arbiter = await Arbiter.deployed();

      await arbiter.addTask(1, "my python generators; my python tests",
          {from: miner});
  });

  it("tasks can't be redefined", async function() {
      let miner = accounts[2];
      await reverting(
          arbiter.addTask(1, "other generators and tests",
          {from: miner})
      );
  });

  // it("should call a function that depends on a linked library", function() {
  //   var meta;
  //   var ArbiterBalance;
  //   var ArbiterEthBalance;
  //
  //   return Arbiter.deployed().then(function(instance) {
  //     meta = instance;
  //     return meta.getBalance.call(accounts[0]);
  //   }).then(function(outCoinBalance) {
  //     ArbiterBalance = outCoinBalance.toNumber();
  //     return meta.getBalanceInEth.call(accounts[0]);
  //   }).then(function(outCoinBalanceEth) {
  //     ArbiterEthBalance = outCoinBalanceEth.toNumber();
  //   }).then(function() {
  //     assert.equal(ArbiterEthBalance, 2 * ArbiterBalance, "Library function returned unexpected function, linkage may be broken");
  //   });
  // });
  // it("should send coin correctly", function() {
  //   var meta;
  //
  //   // Get initial balances of first and second account.
  //   var account_one = accounts[0];
  //   var account_two = accounts[1];
  //
  //   var account_one_starting_balance;
  //   var account_two_starting_balance;
  //   var account_one_ending_balance;
  //   var account_two_ending_balance;
  //
  //   var amount = 10;
  //
  //   return Arbiter.deployed().then(function(instance) {
  //     meta = instance;
  //     return meta.getBalance.call(account_one);
  //   }).then(function(balance) {
  //     account_one_starting_balance = balance.toNumber();
  //     return meta.getBalance.call(account_two);
  //   }).then(function(balance) {
  //     account_two_starting_balance = balance.toNumber();
  //     return meta.sendCoin(account_two, amount, {from: account_one});
  //   }).then(function() {
  //     return meta.getBalance.call(account_one);
  //   }).then(function(balance) {
  //     account_one_ending_balance = balance.toNumber();
  //     return meta.getBalance.call(account_two);
  //   }).then(function(balance) {
  //     account_two_ending_balance = balance.toNumber();
  //
  //     assert.equal(account_one_ending_balance, account_one_starting_balance - amount, "Amount wasn't correctly taken from the sender");
  //     assert.equal(account_two_ending_balance, account_two_starting_balance + amount, "Amount wasn't correctly sent to the receiver");
  //   });
  // });
});
